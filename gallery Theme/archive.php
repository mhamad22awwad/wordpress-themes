<?php get_header(); ?>



    <div class="w3-col s9">

        <div class="w3-row">

            <h1 class="wp-single-cats">
                <?php 
                    if(is_category()){
                        single_cat_title();
                    } elseif(is_author()){
                        the_post();
                        echo "Archives By Author " . get_the_author();
                        rewind_posts();
                    }elseif(is_tag()){
                        single_tag_title();
                    }elseif(is_day()){
                        echo "Archives By Day: " . get_the_date();
                    }elseif(is_month()) {
                        echo "Archives By Month: " . get_the_date( "F Y" );
                    }elseif(is_year()){
                        echo "Archives By year: " . get_the_date( "Y" );
                    }else{
                        echo "Archives";
                    }
                ?>
            </h1>

            <?php if(have_posts()): ?>

                <?php while (have_posts()): the_post(); ?>

                    <h1><?php the_title(); ?></h1>

                <?php endwhile; ?>

            <?php endif; ?>

        </div>

    </div>








<?php get_footer(); ?>