<?php get_header(); ?>


        <div class="wp-post w3-col l9 m9 s12">

            <div class="w3-row">

                <?php if(have_posts()): ?>
                    <?php while(have_posts()): the_post(); ?>
                    
                    
                        <?php get_template_part( "content" , get_post_format() ); ?>
                    

                    <?php endwhile; ?>
                <?php endif; ?>

            </div>


        </div>


<?php get_footer(); ?>