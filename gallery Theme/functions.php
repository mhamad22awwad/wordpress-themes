<?php 

require_once("widgets/class-wp-widget-categories.php");


function theme_setup(){

    /**
     * add img to the posts
     */
    add_theme_support( "post-thumbnails" );

    set_post_thumbnail_size( 600 , 900 );


    add_theme_support( 
        "post-formats" , 
        array( 
            "gallery"
            ));


}

add_action( "after_setup_theme" , "theme_setup" );


/**
 * this function will add sidebar to the admin panel
 * and will give some html & css to in the widget
 */
function side_widget($id){

    register_sidebar(array(
        "name"          => "the side bar in left",
        "id"            => "sidebar_left",
        "before_widget" => "<div class='w3-ul wp-side-bar'>",
        "after_widget"  => "</div>",
        "before_title"  => "<h3 class=''>",
        "after_title"   => "</h3>"
    ));


}

add_action( "widgets_init" , "side_widget" );


function custom_register_widgets(){
    register_widget( "WP_Widget_Categories_Custom" );
}

add_action( "widgets_init" , "custom_register_widgets" );



?>