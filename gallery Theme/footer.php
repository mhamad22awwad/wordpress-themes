</div>


<footer class="w3-container w3-center">

    <p>
        <?php bloginfo( "name" ); ?> 
        &copy;
        <?php the_time( "Y" ); ?>
    </p>

</footer>

<?php wp_footer(); ?>

</body>

</html>