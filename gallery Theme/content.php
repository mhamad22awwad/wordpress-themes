<?php if(is_home()): ?>


    <article class="wp-post w3-col l4 m6 s12">

        <h3> in 
            <?php 
                            
                $cats   = get_the_category();
                $space  = ", ";
                $output = "";

                if($cats){
                    foreach ( $cats as $cat ) {
                        $output .= "<a href='".get_category_link($cat->term_id)." ' class='wp-cats' /> ". $cat->cat_name ." </a> " . $space ;
                    }
                }

                echo trim($output,$space);

            ?>
        </h3>
        <a href="<?php permalink_link(); ?>">
            <h1 class="wp-title" >
                <?php the_title(); ?>
            </h1>

            <div class="wp-text">
                <?php the_excerpt(); ?>
            </div>
        </a>
    </article>

<?php elseif(is_single()): ?>

    <article class="wp-singel-post">

        <div class="wp-singel-title">
            <h1><?php the_title(); ?></h1>
        </div>

                

        <div class="wp-singel-text">
            <?php the_content(); ?>
        </div>

        <div class="wp-back">
            <a href="<?php echo site_url(); ?>">Back</a>
        </div>

    </article>


<?php endif; ?>