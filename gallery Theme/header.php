<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( "charset" ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <link rel="stylesheet" href="<?php bloginfo( "stylesheet_url" ) ?>">

    <?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>



<header class="wp-header w3-container w3-teal" >
    
    <div class="w3-row">
        <div class="w3-col l10 s6">
            <h1 class="wp-width ">
                <?php bloginfo( "name" ); ?>
            </h1>
        </div>        
        <div class="w3-col l2 s6">
            <form method="get" action="<?php esc_url(home_url( "/" )) ?>" class="">
                <input type="text" name="s" id="s" placeholder="searche..." >
            </form>
        </div>
            
    </div>

</header>



<div class="w3-row">

    <div class="wp-post w3-col l3 m3 s10 w3-container">

        <?php if(is_active_sidebar( "sidebar_left" )): ?>
            <?php dynamic_sidebar( "sidebar_left" ); ?>
        <?php endif; ?>

    </div>