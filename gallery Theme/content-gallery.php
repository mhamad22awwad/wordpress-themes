<?php if(is_home()): ?>

    <article class="wp-post w3-col l4 m6 s12">
        <h3> in 
            <?php 
                            
                $cats   = get_the_category();
                $space  = ", ";
                $output = "";

                if($cats){
                    foreach ( $cats as $cat ) {
                        $output .= "<a href='".get_category_link($cat->term_id)." ' class='wp-cats' /> ". $cat->cat_name ." </a> " . $space ;
                    }
                }

                echo trim($output,$space);

            ?>
        </h3>
        <?php 
        
            $attr = array(
                "class" => "w3-animate-opacity w3-hover-opacity"
            );
        
        ?>
        <a href="<?php permalink_link(); ?>">
            <div class="wp-img" >
                <?php the_post_thumbnail("thumbnail", $attr); ?>
            </div>

            <div class="wp-text">
                <?php the_excerpt(); ?>
            </div>
        </a>
    </article>

<?php elseif(is_single()): ?>

    <article class="wp-singel-post">


        <div class="wp-singel-img w3-animate-zoom">
            <?php the_post_thumbnail(); ?>
        </div>

        <div class="wp-singel-title">
            <h1><?php the_title(); ?></h1>
        </div>

        <div class="wp-singel-text">
            <?php the_content(); ?>
        </div>

        <div class="wp-back">
            <a href="<?php echo site_url(); ?>">Back</a>
        </div>

    </article>


<?php endif; ?>