<?php 

require_once("class-wp-bootstrap-navwalker.php");



function theme_setup(){

    register_nav_menus(array(
        "primary"=>"the top nav in all pages"
    ));

    add_theme_support( "custom-logo" );

}

add_action( "after_setup_theme" , "theme_setup" );


function business_customize_register($wp_customize){

    // Banner Section
    $wp_customize->add_section("banner" , array(
        "title" => __("Banner" , "business"),
        "description" => sprintf(__("Options for homepage banner", "business")),
        "priority" => 130
    ));

    // Heading setting
    $wp_customize->add_setting("banner_heading" , array(
        "default"   =>_x( "Banner Heading" , "business" ),
        "type"      => "theme_mod"
    ));

    // Heading Control
    $wp_customize->add_control("banner_heading" , array(
        "label"     =>__( "Heading" , "business" ),
        "section"   => "banner",
        "priority"  => 20
    ));

    // Text setting
    $wp_customize->add_setting("banner_text" , array(
        "default"   =>_x( "Lorem Ipsum is simply dummy text of 
                            the printing and typesetting industry. Lorem Ipsum has 
                            been the industry's standard dummy text ever since the 
                            1500s, when an unknown printer took a galley of type and 
                            scrambled it to make a type specimen book. It has survived 
                            not only five centuries, but also the leap into electronic 
                            typesetting, remaining essentially unchanged. 
                            It was popularised in the 1960s with the release of 
                            Letraset sheets containing Lorem Ipsum passages, 
                            and more recently with desktop publishing software 
                            like Aldus PageMaker including versions of Lorem Ipsum." , "business" ),
        "type"      => "theme_mod"
    ));

    // Text control 
    $wp_customize->add_control( "banner_text" , array(
        "label"     =>__( "Text" , "business" ),
        "section"   => "banner",
        "priority"  => 20
    ));

    // button text setting
    $wp_customize->add_setting("banner_btn_text" , array(
        "default"   =>_x( "Sign Up" , "business"),
        "type"      => "theme_mod"
    ));

    // button text control
    $wp_customize->add_control("banner_btn_text" , array(
        "label"     =>__( "Button Text" , "business" ),
        "section"   => "banner",
        "priority"  => 20
    ));

    // button url setting
    $wp_customize->add_setting("banner_btn_url" , array(
        "default"   =>_x( "#" , "business" ),
        "type"      => "theme_mod"
    ));

    // button url control
    $wp_customize->add_control("banner_btn_url" , array(
        "label"     =>__( "Button Url" , "business" ),
        "section"   => "banner",
        "priority"  => 20
    ));

    // backgraound image setting
    $wp_customize->add_setting("banner_image" , array(
        "default"   => get_bloginfo( "template_directory" ). "/img/banner.jpg",
        "type"      => "theme_mod"
    ));

    $wp_customize->add_control(new WP_Customize_Image_Control(
        $wp_customize , "banner_image" , array(
            "label"     => __("Background Image", "business" ),
            "section"   => "banner",
            "settings"  => "banner_image"
    )));




    // add boxs section
    $wp_customize->add_section("boxs" , array(
        "title"         => __("All Boxs " , "business"),
        "description"   => sprintf(__("The Content in the 3 boxs" , "business")),
        "preoraty"      => 130
    ));

    // add box 1 header setting
    $wp_customize->add_setting("box_1_head" , array(
        "default"   => _x( "Box 1 Header", "business" ),
        "type"      => "theme_mod"
    ));

    // add box 1 header control
    $wp_customize->add_control("box_1_head" , array(
        "label"     => __("Header for Box num 1" , "business" ),
        "section"   => "boxs",
        "priority"  => 20
    ));

    // add box 1 text setting
    $wp_customize->add_setting("box_1_text" , array(
        "default"   => _x( "Box 1 text" , "business" ),
        "type"      => "theme_mod"
    ));

    // add box 1 text control
    $wp_customize->add_control("box_1_text" , array(
        "label"     => __( "Text in the Box num 1" , "business" ),
        "section"   => "boxs",
        "priority"  => 20
    ));

    // add box 2 header setting
    $wp_customize->add_setting("box_2_head" , array(
        "default"   => _x( "Box 2 header" , "business" ),
        "type"      => "theme_mod"
    ));

    // add box 2 header control
    $wp_customize->add_control("box_2_head" , array(
        "label"     => __( "Header for box num 2" , "business" ),
        "section"   => "boxs",
        "priority"  => 20
    ));



}

add_action( "customize_register" , "business_customize_register" );