<?php get_header(); ?>

    <main>
        <div class="container">
            <div class="row">
                <div class="col-9">
                    <h1>search result</h1>
                    <?php if(have_posts()): ?>
                        <?php while(have_posts()):the_post(); ?>
                            <?php get_template_part( "content" , get_post_format()); ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
                <div class="sidebar col-3">
                </div>
            </div>
        </div>
    </main>

<?php  get_footer(); ?>