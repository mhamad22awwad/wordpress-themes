<?php get_header(); ?>


<main>
    <div class="container">
        <div class="row">

            <div class="main col-lg-9 col-md-12 col-sm-12">

                <h1>
                    <?php 
                        if(is_category()){
                            single_cat_title();
                        } elseif(is_author()){
                            the_post();
                            echo "Archives By Author " . get_the_author();
                            rewind_posts();
                        }elseif(is_tag()){
                            single_tag_title();
                        }elseif(is_day()){
                            echo "Archives By Day: " . get_the_date();
                        }elseif(is_month()) {
                            echo "Archives By Month: " . get_the_date( "F Y" );
                        }elseif(is_year()){
                            echo "Archives By year: " . get_the_date( "Y" );
                        }else{
                            echo "Archives";
                        }
                    ?>
                </h1>

                
                <?php if(have_posts()): ?>

                    <?php while(have_posts()):the_post(); ?>

                        <?php get_template_part( "content" , get_post_format()); ?>

                    <?php endwhile; ?>

                <?php endif; ?>

            </div>

            
            <div class="sidebar col-lg-3 col-md-12 col-sm-12">

                <div class="container">

                    <?php if(is_active_sidebar("sidebar_num1")): ?>
                        
                        <?php dynamic_sidebar("sidebar_num1"); ?>
                        
                    <?php endif; ?>

                </div>
            </div>
        
        </div>
    </div>

</main>

<?php  get_footer(); ?>