<?php get_header(); ?>


    <main class="main mb-5">
        

            
        <?php if(is_active_sidebar("show_case")): ?>
            <?php dynamic_sidebar("show_case"); ?>
        <?php endif; ?>
            

        <div class="container mb-5">
            <div class="row">
                <div class="main col-12 p-0">

                        <?php if(have_posts()): ?>
                        
                            <?php while(have_posts()): the_post(); ?>
                            
                            
                                <article class="post container bg-light border mb-4">                                    
                                    <h2 class="mt-3 pl-2 "> <?php the_title(); ?> </h2>
                                    <div class="px-5">
                                        <?php the_content(); ?>
                                    </div>
                                </article>
                            
                            <?php endwhile; ?>
                        
                        <?php else: ?>

                            <?php echo wpautop("Sorry, No Posts"); ?>
                        
                        <?php endif; ?>

                </div>
            </div>
        </div>
        

        <div class="container">
            <div class="row">

                    <?php if(is_active_sidebar("box_num_1")): ?>
                        <?php dynamic_sidebar("box_num_1"); ?>
                    <?php endif; ?>

                    <?php if(is_dynamic_sidebar("box_num_2")): ?>
                        <?php dynamic_sidebar("box_num_2"); ?>
                    <?php endif; ?>

                    <?php if(is_active_sidebar("box_num_3")): ?>
                        <?php dynamic_sidebar("box_num_3"); ?>
                    <?php endif; ?>



                </div>
            </div>
        </div>


    </main>





<?php get_footer(); ?>