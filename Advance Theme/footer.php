
    <footer class="py-3 bg-dark d-flex justify-content-around">

        <p class="text-white">&copy; <?php the_time( "Y" ); ?> - WordPressDev</p>
        
        <nav class="nav">    
            <?php $args = array(
                        "theme_location"  => "footer_nav",
                        "menu_class"      => "nav justify-content-end",
                        "container"       => "",
                        "depth"         => 1,
                        "fallback_cb"   => false,
                        "add_li_class"  => "nav-item text-white nav-link"
                        ); ?>
            <?php wp_nav_menu( $args ); ?>
        </nav>

    </footer>



<?php wp_footer(); ?>

</body>
</html>