<article class="post border-bottom">
        <h2 class="mt-3"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        <h4 class="mb-2">
            writen by <a href="<?php echo get_author_posts_url(get_the_author_meta('id')); ?>"> <?php the_author(); ?> </a> 
            on <?php the_time("F j, Y g:i a"); ?>
            in 
            <?php 
                /**
                 * this code will display all cat htat the post is in them
                 */
                $cats = get_the_category();
                $seprate = ", ";
                $output = "";

                if($cats){
                    foreach ($cats as $cat) {
                        $output .= "<a href='". get_category_link( $cat->term_id ) ." ' /> ". $cat->cat_name ." </a>" . $seprate ;
                    }
                }

                echo trim($output , $seprate);

            ?>
        </h4>
        <div class="px-5">
            <?php the_content(); ?>
        </div>
        <a href="<?php the_permalink(); ?>" class="btn btn-success mb-3"> Read More </a>
    </article>