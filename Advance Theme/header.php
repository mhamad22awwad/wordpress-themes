<!DOCTYPE html>
<html <?php language_attributes(); ?> >
<head>
    <meta charset="<?php bloginfo("charset"); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php bloginfo("name"); ?></title>
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="<?php bloginfo("stylesheet_url"); ?>">

    <?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

    <header class="bg-light">
        <div class="container">
            
            <div class="d-flex bd-highlight">
                <div class="p-2 flex-grow-1 bd-highlight ">
                    <h1 class="" id="basic-addon1"><a href="<?php echo home_url( "/" ); ?>"><?php bloginfo( "name" ); ?></a> <small><?php bloginfo( "description" ); ?></small></h1>
                </div>
                <div class="p-2 bd-highlight">
                    <form method="get" action="<?php esc_url(home_url("/")); ?>">
                        <input type="text" name="s" class="form-control" placeholder="searche..." >
                    </form>
                </div>
            </div>
            
        </div>   
    </header>
    
    <nav class="nav bg-dark text-white mb-4">
        <?php $args = array(
                        "theme_location"  => "top_nav",
                        "menu_class"      => "container nav",
                        "container"       => "",
                        "depth"         => 1,
                        "fallback_cb"   => false,
                        "add_li_class"  => "nav-item text-white nav-link"
                        
                        
                            ); ?>
        <?php wp_nav_menu( $args ); ?>
    </nav>