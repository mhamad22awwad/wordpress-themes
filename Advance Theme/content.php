<?php if(is_home()): ?>

    <article class="post border-bottom">
        <h2 class="mt-3"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        <h4 class="mb-2">
            writen by <a href="<?php echo get_author_posts_url(get_the_author_meta('id')); ?>"> <?php the_author(); ?> </a> 
            on <?php the_time("F j, Y g:i a"); ?>
            in 
            <?php 
                /**
                 * this code will display all cat htat the post is in them
                 */
                $cats = get_the_category();
                $seprate = ", ";
                $output = "";

                if($cats){
                    foreach ($cats as $cat) {
                        $output .= "<a href='". get_category_link( $cat->term_id ) ." ' /> ". $cat->cat_name ." </a>" . $seprate ;
                    }
                }

                echo trim($output , $seprate);

            ?>
        </h4>
        <div class="px-5">
            <p><?php the_excerpt(); ?></p>
        </div>
        <a href="<?php the_permalink(); ?>" class="btn btn-success mb-3"> Read More </a>
    </article>
    
<?php elseif(is_single()): ?>


    <h1> <?php the_title(); ?> </h1>

    <div class="row">

        <div class="col-lg-6 col-md-5 col-sm-12">

            <div>
                <?php the_content(); ?>
            </div>

        </div>

        <div class="col-lg-6 col-md-7 col-sm-12">
            <?php if(has_post_thumbnail()): ?>

                <div class="img-thumbnail">
                    <?php the_post_thumbnail(); ?>
                </div>

            <?php endif; ?>
        </div>

    </div>
    <h4>by <?php the_author(); ?> on <?php the_date(); ?></h4>


<?php elseif(is_page()): ?>


    <article class="post border-bottom wp-page">

        <?php if(page_is_parent() || $post->post_parent > 0): ?>
            <nav class="bg-light">
                
                <ul class="nav mini-nav justify-content-end wp-text-size">
                    <span class="nav-item ">
                        <a class="nav-link active text-dark" href="<?php echo get_the_permalink(get_top_psrent()); ?>">
                            <?php echo get_the_title(get_top_psrent()); ?>
                        </a>
                    </span>

                    <?php 
                        $args = array(
                            "child_of" => get_top_psrent(),
                            "title_li" => ""
                        ); 
                    ?>
                    <?php wp_list_pages( $args ); ?>
                </ul>
            </nav>
        <?php endif; ?>
        
        <h2 class="mt-3 abs"> <?php the_title(); ?> </h2>
        <div class="px-5">
            <p><?php the_content(); ?></p>
        </div>
    </article>


<?php elseif(is_archive()): ?>

    <div class="card mb-3 bg-light">
        <h1 class="card-title p-2"><a href="<?php  the_permalink(); ?>"> <?php the_title(); ?> </a></h1>
        <h2 class="card-body">The Post is for <?php the_author(); ?> </h2>
        <h3 class="card-footer mb-0">Posted on <?php the_date(); ?> </h3>
    </div>


<?php elseif(is_search()): ?>


    <div class="card mb-3 bg-light">
        <h1 class="card-title p-2"><a href="<?php  the_permalink(); ?>"> <?php the_title(); ?> </a></h1>
        <h2 class="card-body">The Post is for <?php the_author(); ?> </h2>
        <h3 class="card-footer mb-0">Posted on <?php the_date(); ?> </h3>
    </div>


<?php endif; ?>


