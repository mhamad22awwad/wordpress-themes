<?php 


/**
 * this function will add some set up to the admin panel
 */
function theme_setup(){
    /**
     * add nav menus 
     */
    register_nav_menus(array(
        "top_nav"    => __("this nav will be in the top of the all pages"),
        "footer_nav" => __("this nav will be in the footer for all pages")
    ));

    /**
     * add img 
     */
    add_theme_support( "post-thumbnails" );

    /**
     * add all type of post formats in to the theme
     */
    add_theme_support( 
        "post-formats" , 
        array( 
            "aside" , "gallery" , "link" , 
            "image" , "quote" , "status" , 
            "video" , "audio" , "chat"
            ));

}

add_action( "after_setup_theme" , "theme_setup" );


/**
 * this function will add sidebar to the admin panel
 * and will give some html & css to in the widget
 */
function sidebar_widget($id){

    // side widget 
    register_sidebar(array(
        "name"          => "the side bar",
        "id"            => "sidebar_num1",
        "before_widget" => "<div class='card p-2 mb-3 bg-light wp-side-bar'>",
        "after_widget"  => "</div>",
        "before_title"  => "<h3 class='card-title'>",
        "after_title"   => "</h3>"

    ));
    // widget in the top of the home page
    register_sidebar(array(
        "name"          => "show case",
        "id"            => "show_case",
        "before_widget" => "<div class='container bg-info p-5 mb-4 text-center'>",
        "after_widget"  => "</div>",
        "before_title"  => "<h1>",
        "after_title"   => "</h1>"

    ));
    
    // a box widget
    register_sidebar(array(
        "name"          => "box num 1",
        "id"            => "box_num_1",
        "before_widget" => "<div class='col-lg-4 col-md-12 col-sm-12  p-3 mb-md-2 border wp-side-bar'>",
        "after_widget"  => "</div>",
        "before_title"  => "<h3 class='bg-info p-2'>",
        "after_title"   => "</h3>"

    ));

    register_sidebar(array(
        "name"          => "box num 2",
        "id"            => "box_num_2",
        "before_widget" => "<div class='col-lg-4 col-md-12 col-sm-12  p-3 mb-md-2 border wp-side-bar'>",
        "after_widget"  => "</div>",
        "before_title"  => "<h3 class='bg-info p-2'>",
        "after_title"   => "</h3>"

    ));

    register_sidebar(array(
        "name"          => "box num 3",
        "id"            => "box_num_3",
        "before_widget" => "<div class='col-lg-4 col-md-12 col-sm-12  p-3 mb-md-2 border wp-side-bar'>",
        "after_widget"  => "</div>",
        "before_title"  => "<h3 class='bg-info p-2'>",
        "after_title"   => "</h3>"

    ));

}

add_action( "widgets_init" , "sidebar_widget" );



/**
 * in this function it will add class to li in nav
 */
function add_additional_class_on_li($classes, $item, $args) {
    if($args->add_li_class) {
        $classes[] = $args->add_li_class;
    }
    return $classes;
}

add_filter('nav_menu_css_class', 'add_additional_class_on_li', 1, 3);



/**
 * this function will see if there a pages related to the dig bage
 * pareants page and son pages
 */
function get_top_psrent(){

    global $post;
    if($post->post_parent){
        $ancestors = get_post_ancestors( $post->ID );
        return $ancestors[0];
    }

    return $post->ID;
}


/**
 * this will add the (nav-link active) classes to the mini parents nav
 * \" class names \"
 */
add_filter('wp_list_pages', create_function('$t', 'return str_replace("<a ", "<a class=\"nav-link active\" ", $t);'));


/**
 * this function will return the num of son for every gage
 */
function page_is_parent(){
    global $post;

    $pages = get_pages("child_of=" . $post->ID);
    
    return count($pages);
}


?>