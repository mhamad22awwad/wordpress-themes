<?php
// show the one post
?>
<?php  get_header(); ?>


        <div class="container">
            <div class="row">

                <div class="main col-lg-9 col-md-12 col-sm-12">

                    <div class="container">

                        <?php if(have_posts()): ?>

                            <?php while(have_posts()) : the_post(); ?>

                                <?php get_template_part( "content" , get_post_format()); ?>

                            <?php endwhile; ?>
                            <?php else: ?>

                            <?php wpautop("sorry no data") ?>

                        <?php endif; ?>

                        <div>
                            <?php comments_template(); ?>
                        </div>

                    </div>

                </div>



                <div class="sidebar col-lg-3 col-md-12 col-sm-12">

                    <div class="container">

                        <?php if(is_active_sidebar("sidebar_num1")): ?>
                            
                            <?php dynamic_sidebar("sidebar_num1"); ?>
                            
                        <?php endif; ?>
                    
                    </div>

                </div>



            </div>
        </div>

        


<?php  get_footer(); ?>