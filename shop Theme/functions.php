<?php 

require_once("class-wp-bootstrap-navwalker.php");


function theme_setup(){

    // add theme logo 
    add_theme_support( "custom-logo" );

    // add a nav menus 
    register_nav_menus(array(
        "primary" => "the nav bar in top of pages"
    ));

    add_theme_support( "post-thumbnails" );

}

add_action( "after_setup_theme" , "theme_setup" );


// add sied widgets
function widgets($id){
    // a top widgets in the home page
    register_sidebar( array(
        "name"          => "show case",
        "id"            => "show_case",
        "before_widget" => "<div class='text-center p-4 container-fluid mx-auto w-75 card bg-light'>",
        "after_widget"  => "</div>",
        "before_title"  => "<h2 class='p-3'>",
        "after_title"   => "</h2>"
    ));
    //  a side widgets in all pages
    register_sidebar( array(
        "name"          => "side bar",
        "id"            => "side_bar",
        "before_widget" => "<div class='p-4 mb-4 container mx-auto w-100 card bg-light'>",
        "after_widget"  => "</div>",
        "before_title"  => "<h2 class='p-3'>",
        "after_title"   => "</h2>"
    ));

}

add_action( "widgets_init" , "widgets" );