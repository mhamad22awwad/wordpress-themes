<?php get_header(); ?>



<header class="my-5">
    <?php if(is_active_sidebar("show_case")): ?>
        <?php dynamic_sidebar("show_case"); ?>
    <?php endif; ?>
</header>



<div class="container-fluid">
    <div class="row">

        <div class="col-lg-8 ml-lg-5 col-md-9 col-sm-12">
            <div class="row">
                <?php if(have_posts()): ?>
                    <?php while(have_posts()): the_post(); ?>

                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <h3 class="pl-4"><?php the_title(); ?></h3>
                            
                            <?php if(has_post_thumbnail()): ?>
                                <?php 
                                
                                    $attr = array(
                                        "class" => "wp-img"
                                    );
                                
                                ?>
                                <div class="p-3">
                                    <?php the_post_thumbnail("thumbnail", $attr); ?>
                                </div>
                            <?php endif; ?>

                            <div class="text-center">
                                <a class="btn btn-light w-50" href="<?php echo the_permalink(); ?>">Show</a>
                            </div>

                        </div>

                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>


        <div class="col-lg-3 ml-lg-5 col-md-3 col-sm-12">
            <?php if(is_active_sidebar("side_bar")): ?>
                <?php dynamic_sidebar("side_bar"); ?>
            <?php endif; ?>
        </div>

    </div>
</div>






<?php get_footer(); ?>