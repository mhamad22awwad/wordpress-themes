<?php get_header(); ?>



<div class="container">
    <div class="row">

        <div class="col-lg-9 col-md-9 col-sm-12">

            <?php if(have_posts()): ?>
                <?php while(have_posts()):the_post(); ?>

                    <h1><?php the_title(); ?></h1>
                    <div>
                        <?php the_content(); ?>
                    </div>
               
                <?php endwhile; ?>
            <?php endif; ?>

            

        </div>

        
    </div>
</div>







<?php get_footer(); ?>