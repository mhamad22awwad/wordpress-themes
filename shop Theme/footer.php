


<footer class="text-center mt-5 mb-5">
    <div class="container border-top pt-3">
    <h3>
        <?php bloginfo( "name" ); ?>
        &copy;
        <?php the_time( "Y" ); ?>
    </h3>    
        
    </div>
</footer>



<?php wp_footer(); ?>
</body>
</html>