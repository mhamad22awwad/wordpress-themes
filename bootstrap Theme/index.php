<?php get_header(); ?>

    
        
        <div class="col-lg-8 col-md-8 col-sm-12">

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">blog posts</h3>
                </div>
                <div class="card-body">
                    <?php if(have_posts()): ?>

                        <?php while(have_posts()):the_post(); ?>

                            <?php if(has_post_thumbnail()): ?>

                                <div class="row border-bottom mb-4">
                                    <div class="col-lg-3 col-md-3 col-sm-12">
                                        
                                            <div class="">
                                                <?php $attr = array("class" => "wp-img") ?>
                                                <?php the_post_thumbnail("thumbnail",$attr); ?>
                                            </div>
                                        
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-12">
                                        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> At <small><?php the_time(); ?></small> on <small><?php the_date(); ?></small></h2>
                                        <h3>By <?php the_author(); ?></h3>
                                        <div class="wp-text">
                                            <?php the_excerpt(); ?>
                                        </div>
                                        <div class="wp-more text-center mb-3">
                                            <a href="<?php the_permalink(); ?>" class="btn btn-light w-75">Read More &raquo;&raquo;</a>
                                        </div>
                                    </div>
                                </div>

                            <?php else: ?>

                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> At <small><?php the_time(); ?></small> on <small><?php the_date(); ?></small></h2>
                                    <h3>By <?php the_author(); ?></h3>
                                    <div class="wp-text">
                                        <?php the_excerpt(); ?>
                                    </div>
                                    <div class="wp-more text-center mb-3">
                                        <a href="<?php the_permalink(); ?>" class="btn btn-light w-75 wp-button"> Read More &raquo;&raquo; </a>
                                    </div>
                                </div>

                            <?php endif; ?>
                            

                        <?php endwhile; ?>

                    <?php endif; ?>
                </div>
            </div>
        
        </div>


        <div class="col-lg-4 col-md-4 col-sm-12">

            <?php if(is_active_sidebar("sidebar")): ?>
                <?php dynamic_sidebar("sidebar"); ?>
            <?php endif; ?>
        
        </div>

<?php get_footer(); ?>