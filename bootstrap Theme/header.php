<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo("charset"); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php bloginfo( "name" ); ?></title>
    <link rel="stylesheet" href="<?php bloginfo("template_directory"); ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php bloginfo("template_directory"); ?>/css/style.css">
</head>
<body>


<nav class="navbar navbar-expand-lg navbar-light bg-light mb-5" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <a class="navbar-brand" href="<?php echo home_url( "/" ); ?>"><?php bloginfo("name"); ?></a>
    
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".bs-example-navbar-collapse-1" aria-controls="bs-example-navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	
		<?php
		wp_nav_menu( array(
			'theme_location'    => 'topnav',
			'depth'             => 2,
			'container'         => 'div',
			'container_class'   => 'collapse navbar-collapse bs-example-navbar-collapse-1',
			'container_id'      => 'bs-example-navbar-collapse-1',
			'menu_class'        => 'nav navbar-nav',
			'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
			'walker'            => new WP_Bootstrap_Navwalker(),
		) );
        ?>
        
        <form class="form-inline my-2 my-lg-0 collapse navbar-collapse bs-example-navbar-collapse-1"  method="get" action="<?php esc_url(home_url("/")); ?>" >
            <input class="form-control mr-sm-2" name="s" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
	</div>
</nav>


<div class="container">
    <div class="row">
    