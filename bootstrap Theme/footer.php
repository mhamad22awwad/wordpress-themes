
        


    </div>
</div>

<footer class="text-center bg-light mt-5">
    <div class="container p-5">
        <h2>    
            <span class="wp-name">
                <?php bloginfo("name"); ?>
            </span>
            &copy;
            <span class="wp-yeare">
                <?php the_time( "Y" ); ?>
            </span>
        </h2>
    </div>
</footer>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="<?php bloginfo("template_directory"); ?>/js/bootstrap.js"></script>

<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "";
  document.getElementById("mySidenav").classList.add("sidenav-width");
  
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "";
  document.getElementById("mySidenav").classList.remove("sidenav-width");
}
</script>
  
</body>
</html>
