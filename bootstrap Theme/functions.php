<?php

require_once("class-wp-bootstrap-navwalker.php");

require_once("widgets/class-wp-widget-categories.php");
require_once("widgets/class-wp-widget-recent-comments.php");
require_once("widgets/class-wp-widget-recent-posts.php");
require_once("widgets/class-wp-widget-text.php");

function theme_setup(){

    register_nav_menus(array(
        "topnav" => __("this it the top nav bar it top of all pages")
    ));

    add_theme_support( "post-thumbnail" );


}

add_action("after_setup_theme" , "theme_setup");

function init_widget($id) {

    register_sidebar(array(
        "name"         =>"sidebar",
        "id"           =>"sidebar",
        "before_widget"=>"<div class='card wp-wiget-sidebar'>",
        "after_widget" =>"</div>",
        "before_title" =>"<h2 class='card-header p-3'>",
        "after_title"  =>"</h2>"
    ));

}

add_action( "widgets_init" , "init_widget" );


/**
 * in this function it will add class to li in nav
 */
function add_additional_class_on_li($classes, $item, $args) {
    if($args->add_li_class) {
        $classes[] = $args->add_li_class;
    }
    return $classes;
}

add_filter('nav_menu_css_class', 'add_additional_class_on_li', 1, 3);


/**
 * 
 */
function wp_register_widgets(){
    register_widget( "WP_Widget_Categories_Custom" );
    register_widget( "WP_Widget_Recent_Comments_Custom" );
    register_widget( "WP_Widget_Recent_Posts_Custom" );
    register_widget( "WP_Widget_Text_Custom" );

}

add_action( "widgets_init" , "wp_register_widgets" );

/**
 * add class to categores li
 */
function add_new_class_list_cats($list){
    $list = str_replace("cat-item" , "cat-item list-group-item" , $list);
    return $list;
}

add_filter( "wp_list_categories" , "add_new_class_list_cats" );


/**
 * Undocumented function
 *
 * @param [type] $comment
 * @param [type] $args
 * @param [type] $depth
 * @return void
 */
function add_theme_comments($comment, $args, $depth) {
    if ( 'div' === $args['style'] ) {
        $tag       = 'div';
        $add_below = 'comment';
    } else {
        $tag       = 'li';
        $add_below = 'div-comment';
    }?>
    <<?php echo $tag; ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ); ?> id="comment-<?php comment_ID() ?>"><?php 
    if ( 'div' != $args['style'] ) { ?>
        <div id="div-comment-<?php comment_ID() ?>" class="comment-body m-3"><?php
    } ?>
        <div class="comment-author vcard"><?php 
            if ( $args['avatar_size'] != 0 ) {
                echo get_avatar( $comment, $args['avatar_size'] ); 
            } 
            printf( __( '<cite class="fn">%s</cite> <span class="says">says:</span>' ), get_comment_author_link() ); ?>
        </div><?php 
        if ( $comment->comment_approved == '0' ) { ?>
            <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></em><br/><?php 
        } ?>
        <div class="comment-meta commentmetadata">
            <a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>"><?php
                /* translators: 1: date, 2: time */
                printf( 
                    __('%1$s at %2$s'), 
                    get_comment_date(),  
                    get_comment_time() 
                ); ?>
            </a><?php 
            edit_comment_link( __( '(Edit)' ), '  ', '' ); ?>
        </div>

        <?php comment_text(); ?>

        <div class="reply"><?php 
                comment_reply_link( 
                    array_merge( 
                        $args, 
                        array( 
                            'add_below' => $add_below, 
                            'depth'     => $depth, 
                            'max_depth' => $args['max_depth'] 
                        ) 
                    ) 
                ); ?>
        </div><?php 
    if ( 'div' != $args['style'] ) : ?>
        </div><?php 
    endif;
}