<?php get_header(); ?>




<div class="col-lg-8 col-md-8 col-sm-12">

    <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; open</span>

    <?php if(have_posts()): ?>
        <?php while(have_posts()): the_post(); ?>

            <article class="wp-singel-post">

                <h1 class="text-center card-header p-3"><?php the_title(); ?></h1>
                
                <?php if(has_post_thumbnail()): ?>

                    <div class="wp-single-img">
                        <?php $attr = array("class"=>""); ?>
                        <?php the_post_thumbnail( "thumbnail" , $attr ); ?>
                    </div>

                <?php endif; ?>

                <div class="wp-single-text mt-4">
                    <?php the_content(); ?>
                </div>

                <div class="wp-back text-center mb-3">
                    <a href="<?php echo home_url( "/" ); ?>" class="btn btn-light w-75 wp-button"> Back Home </a>
                </div>

            </article>

        <?php endwhile; ?>
    <?php endif; ?>

    <div class="container mt-4">
        <?php comments_template(); ?>
    </div>

</div>




<div class="row">

    <div id="mySidenav"  class="sidenav">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        
        <?php if(is_active_sidebar("sidebar")): ?>
        <?php dynamic_sidebar("sidebar"); ?>
        <?php endif; ?>
    </div>
    
</div>










<!-- 
<div id="" class="">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="#">About</a>
  <a href="#">Services</a>
  <a href="#">Clients</a>
  <a href="#">Contact</a>
</div>
 -->












<?php get_footer(); ?>