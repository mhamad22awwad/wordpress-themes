<?php get_header(); ?>

<div class="container">
    <div class="row">

        <div class="main col-lg-9">
            <div class="container">
                <?php if(have_posts()): ?>
                    <?php while(have_posts()): the_post(); ?>
                        <article class="post border-bottom">
                            <h2 class="mt-3"> <?php the_title(); ?> </h2>
                            <div class="px-5">
                                <p><?php the_content(); ?></p>
                            </div>
                        </article>
                    <?php endwhile; ?>
                <?php else: ?>
                    <?php echo wpautop("Sorry, No Posts"); ?>
                <?php endif; ?>
            </div>
        </div>

        <div class="sidebar col-lg-3">

            <?php if(is_active_sidebar("sidebar")): ?>

                <?php dynamic_sidebar("sidebar"); ?>
            
            <?php endif; ?>

        </div>


    </div>
</div>












<?php get_footer(); ?>