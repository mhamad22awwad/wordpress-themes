

<footer id="footer" class="text-center text-white bg-dark">
    <div class="container py-4 ">
        <p><?php the_time("Y"); ?> - for <?php bloginfo("name"); ?></p>
    </div>
</footer>



<?php wp_footer(); ?>
</body>
</html>