<?php


function theme_setup(){

    
    // add the img support to the theme
     
        add_theme_support("post-thumbnails");
    // 
     
    // add menus
        register_nav_menus( array(
            "primary" => __( "Primary Menu" ),
            // create the second menus
            // "second"  => "the second"
            // create the therd menus
            // "therd"  => "the therd"

        ));  


}

add_action("after_setup_theme" , "theme_setup");


/**
 * set how many word show in the index file
 * for evry post
 */
function set_excerpt_length(){
    
    return 20;
}

add_filter( "excerpt_length" , "set_excerpt_length" );


/**
 * this function will enabel the sidebar 
 */
function init_widget($id){
    // creat the first side bar
    register_sidebar( array(
        "name" => "SideBar",
        "id"   => "sidebar",
        "before_widget" => '<div class="">',
        "after_widget"  => "</div>",
        "before_title"  => "<h2>",
        "after_title"   => "</h2>"
    ));

    // creat the second side bar
    // register_sidebar( array(
    //     "name" => "SideBar second",
    //     "id"   => "sidebar_second",
    //     "before_widget" => '<div class="">',
    //     "after_widget"  => "</div>",
    //     "before_title"  => "<h2>",
    //     "after_title"   => "</h2>"
    // ));

    // creat the therd side bar
    // register_sidebar( array(
    //     "name" => "SideBar therd",
    //     "id"   => "sidebar_therd",
    //     "before_widget" => '<div class="">',
    //     "after_widget"  => "</div>",
    //     "before_title"  => "<h2>",
    //     "after_title"   => "</h2>"
    // ));

}

add_action( "widgets_init" , "init_widget" );