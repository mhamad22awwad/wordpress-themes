<?php get_header(); ?>


<header class="bg-dark text-white">
    <div class="container mb-5 pb-3">
        <h1 class="mb-3 py-2"><?php bloginfo('name'); ?></h1>
        <h2><?php bloginfo('description'); ?></h2>
    </div>
</header>

<div class="container">
    <div class="row">
        <div class="main col-lg-9">

            <div class="container">


                <?php if(have_posts()): ?>
                
                    <?php while(have_posts()): the_post(); ?>
                        <article class="post border-bottom">

                            <h2 class="mt-3"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                            <h4 class="mb-2">writen by <a href="<?php get_author_posts_url(get_the_author_meta('ID')); ?>"> <?php the_author(); ?> </a> on <?php the_time("F j, Y g:i a"); ?></h4>
                            <div class="px-5">
                                <p><?php the_excerpt(); ?></p>
                            </div>
                            <a href="<?php the_permalink(); ?>" class="btn btn-success mb-3"> Read More </a>
                        </article>
                    <?php endwhile; ?>
                
                <?php else: ?>

                    <?php echo wpautop("Sorry, No Posts"); ?>
                
                <?php endif; ?>

            </div>

        </div>

        <div class="sidebar col-lg-3">

            <?php if(is_active_sidebar("sidebar")): ?>

                <?php dynamic_sidebar("sidebar"); ?>
                
            <?php endif; ?>

        </div>

    </div>
</div>

<?php get_footer(); ?>