<?php get_header(); ?>


    <main>

        <div class="container">

            <div>

                <?php if(have_posts()): ?>

                    <?php while(have_posts()) : the_post(); ?>

                        <h1> <?php the_title(); ?> </h1>

                        <div class="row">

                            <div class="col-lg-6 col-md-5 col-sm-12">

                                <div>
                                    <?php the_content(); ?>
                                </div>

                            </div>

                            <div class="col-lg-6 col-md-7 col-sm-12">
                                <?php if(has_post_thumbnail()): ?>

                                    <div class="img-thumbnail">
                                        <?php the_post_thumbnail(); ?>
                                    </div>

                                <?php endif; ?>
                            </div>

                        </div>
                        <h4>by <?php the_author(); ?> on <?php the_date(); ?></h4>



                    <?php endwhile; ?>
                <?php else: ?>

                    <?php wpautop("sorry no data") ?>

                <?php endif; ?>

                <?php comments_template(); ?>

            </div>
        </div>




    




    </main>


<?php get_footer(); ?>